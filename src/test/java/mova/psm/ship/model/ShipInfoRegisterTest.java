package mova.psm.ship.model;

import mova.psm.PsmDataConfig;
import mova.psm.ship.ShipUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PsmDataConfig.class, webEnvironment = WebEnvironment.NONE)
public class ShipInfoRegisterTest {

    private static final String[] SHIP_DEFINITIONS = new String[] {
            "PS-XX1:FIVE_MASTS:La Rata:5:SPAIN:5:S+L:3L3L3L3L3L",
            "PS-XX2:FIVE_MASTS:WS ElisabethII:5:ENGLAND:5:S+L:3L3L3L3L3L",
            "PS-XX3:FIVE_MASTS:Le Furet:5:PIRATE:5:S+L:3L3L3L3L3L",
            "PS-XX4:TWO_MASTS:El Chico:5:PIRATE:2:S+L:2S2S",
            "PS-XX5:FIVE_MASTS:Hermione:5:FRANCE:5:S+L:2S3L4L2S2S"
    };

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ShipInfoRegister shipInfoRegister;

    @Autowired
    private ShipInfoRepository shipInfoRepository;

    @Test
    public void test() {
        try {
            ShipInfo shipInfo1 = ShipUtils.parse(SHIP_DEFINITIONS[0]);
            shipInfoRegister.create(shipInfo1);
            ShipInfo shipInfo2 = ShipUtils.parse(SHIP_DEFINITIONS[1]);
            shipInfoRegister.create(shipInfo2);

            ShipInfoEntity shipInfoEntity1 = shipInfoRepository.findAll().get(0);
            System.out.println(shipInfoEntity1.getId());
            System.out.println(shipInfoEntity1.getReference());
            System.out.println(shipInfoEntity1.getCost());

            ShipInfoEntity shipInfoEntity2 = shipInfoRepository.findAll().get(1);
            System.out.println(shipInfoEntity2.getId());
            System.out.println(shipInfoEntity2.getReference());
            System.out.println(shipInfoEntity2.getCost());

            List<?> results = entityManager.createNativeQuery("select * from ship_info;").getResultList();
            for (Object result : results) {
                System.out.println(Arrays.toString((Object[]) result));
            }

            ShipInfoEntity shipInfoEntity3 = shipInfoRepository.findBySimpleNaturalId("PS-XX1").orElseThrow(EntityNotFoundException::new);
            System.out.println(shipInfoEntity3.getId());
            System.out.println(shipInfoEntity3.getReference());
            System.out.println(shipInfoEntity3.getCost());
        } catch (Exception e) {
            Assert.fail();
        }
    }

}
