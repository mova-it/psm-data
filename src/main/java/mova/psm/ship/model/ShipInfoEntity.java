package mova.psm.ship.model;

import mova.psm.nation.Nation;
import mova.psm.ship.ShipType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.Parameter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Cette classe permet de gérer la persistence des informations statiques d'un navire.
 */
@Entity(name = "ship_info")
@Access(AccessType.PROPERTY)
@NaturalIdCache
public class ShipInfoEntity {

    private int id;

    @Transient
    private ShipInfo shipInfo;

    protected ShipInfoEntity() {
        shipInfo = new ShipInfo();
    }

    ShipInfoEntity(ShipInfo shipInfo) {
        this.shipInfo = shipInfo;
    }

    @Id
    @GeneratedValue(generator = "ship-info-sequence-generator")
    @GenericGenerator(
            name = "ship-info-sequence-generator",
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = @Parameter(name = "sequence_name", value = "ship_info_sequence")
    )
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NaturalId
    @Column(length = 10)
    public String getReference() {
        return shipInfo.getReference();
    }

    // Used by JPA
    protected void setReference(String reference) {
        shipInfo.setReference(reference);
    }

    @Enumerated(EnumType.STRING)
//    @Column(columnDefinition = "enum('ONE_MAST', 'TWO_MASTS', 'TWO_MASTS_BR')", nullable = false)
    @Column(length = 15, nullable = false)
    public ShipType getType() {
        return shipInfo.getType();
    }

    // Used by JPA
    protected void setType(ShipType type) {
        shipInfo.setType(type);
    }

    @Column(length = 50, nullable = false)
    public String getName() {
        return shipInfo.getName();
    }

    // Used by JPA
    protected void setName(String name) {
        shipInfo.setName(name);
    }

    @Column(nullable = false)
    public int getCost() {
        return shipInfo.getCost();
    }

    // Used by JPA
    protected void setCost(int cost) {
        shipInfo.setCost(cost);
    }

    @Enumerated(EnumType.STRING)
//    @Column(columnDefinition = "enum('AMERICA', 'BARBARY_CORSAIR', 'CURSED', 'ENGLAND', 'FRANCE', 'JADE_REBELLION', 'MERCENARY', 'PIRATE', 'SPAIN', 'VIKING')", nullable = false)
    @Column(length = 15, nullable = false)
    public Nation getNation() {
        return shipInfo.getNation();
    }

    // Used by JPA
    protected void setNation(Nation nation) {
        shipInfo.setNation(nation);
    }

    @Column(nullable = false)
    public int getCargo() {
        return shipInfo.getCargo();
    }

    // Used by JPA
    protected void setCargo(int cargo) {
        shipInfo.setCargo(cargo);
    }

    @Column(length = 10, nullable = false)
    public String getMove() {
        return shipInfo.getMoveInfos() != null ? shipInfo.getMoveInfos().getInfos() : null;
    }

    // Used by JPA
    protected void setMove(String move) {
        shipInfo.setMoveInfos(new MoveInfos(move));
    }

    @Column(length = 20, nullable = false)
    public char[] getCannons() {
        return shipInfo.getCannonInfos() != null ? shipInfo.getCannonInfos().getInfos() : null;
    }

    // Used by JPA
    protected void setCannons(char[] cannons) {
        shipInfo.setCannonInfos(new CannonInfos(cannons));
    }

    @Transient
    public ShipInfo getShipInfo() {
        return shipInfo;
    }
}
