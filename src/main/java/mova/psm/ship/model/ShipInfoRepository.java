package mova.psm.ship.model;

import mova.jpa.NaturalIdRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipInfoRepository extends NaturalIdRepository<ShipInfoEntity, Integer> {

    @Query("select reference from ship_info")
    List<String> findAllReferences();

}
