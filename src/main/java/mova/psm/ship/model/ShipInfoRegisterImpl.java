package mova.psm.ship.model;

import org.springframework.stereotype.Component;

import javax.persistence.EntityExistsException;
import java.util.List;

@Component
public class ShipInfoRegisterImpl implements ShipInfoRegister {

    private final ShipInfoRepository shipInfoRepository;

    public ShipInfoRegisterImpl(ShipInfoRepository shipInfoRepository) {
        this.shipInfoRepository = shipInfoRepository;
    }

    @Override
    public ShipInfo create(ShipInfo shipInfo) {
        if (shipInfoRepository.existsBySimpleNaturalId(shipInfo.getReference())) throw new EntityExistsException();

        ShipInfoEntity shipInfoEntity = new ShipInfoEntity(shipInfo);
        return shipInfoRepository.save(shipInfoEntity).getShipInfo();
    }

    @Override
    public ShipInfo getShipInfo(String reference) {
        return shipInfoRepository.findBySimpleNaturalId(reference)
                .map(ShipInfoEntity::getShipInfo)
                .orElse(null);
    }

    @Override
    public List<String> getReferences() {
        return shipInfoRepository.findAllReferences();
    }
}
